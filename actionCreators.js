import { asyncActionNames, buildAsyncActions } from '../Common/GlobalActionCreators';

import { browserHistory } from 'react-router'
// creating actions name and action creators
const actionNames = asyncActionNames('LOGIN');
const actionCreators = buildAsyncActions(actionNames);

export function updateUserType(payload) {
  return {
    type: UPDATE_TYPE,
    payload
  }
}
;


export function processForm(formData) {
  debugger
  return function(dispatch) {
    // we can dispatch progress
    return new Promise((resolve, reject) => {
      dispatch(actionCreators.progress());
      HTTP('post', 'login', formData)
        .then((result) => {
          if (result.data && result.data.statusCode == 200 && result.data.message == "Success") {
            setStorage('token', 'Bearer ' + result.data.data.token);
            setStorage('currentUserRole', result.data.data.user.role);
            setStorage('approvedByShipper', JSON.stringify(result.data.data.user.approvedBy ? result.data.data.user.approvedBy : []));
            setStorage('currentUserID', result.data.data.user._id);
            if (result.data.data.user.shipper) {
              setStorage('shipperCompanyName', result.data.data.user.shipper.company_name);
            }
            if (!result.data.data.user.isEmailVerified) {
              setStorage('varified', false);
            } else {
              dispatch({
                type: "UPDATE_TYPE",
                payload: result.data.data.user.role
              });
              setStorage('varified', true);
              if (getStorage('unverfied') != null) {
                removeItem('unverfied');
              }
            }

          }
          // dispatch(actionCreators.success(result.data.data));
          if (!result.data.data.user.isEmailVerified) {
            setStorage('unverfied', result.data.data.token);
            notify.show('Need to varify first', 'error');
            browserHistory.push('/verify-otp');
          } else if (result.data.data.user.role === 'carrier') {
            //default route for Trucker
            setStorage('carrier_MCNumber', result.data.data.user.carrier.MCNumber);
            browserHistory.push('/track');
          } else if (result.data.data.user.role === 'shipper') {
            //default route for Shipper
            browserHistory.push('/create-order')
          } else if (result.data.data.user.role === 'customer') {
            //default route for Customer
            browserHistory.push('/customer-shipments')
          } else if (result.data.data.user.role === 'fleetmanager') {
            //default route for Customer
            browserHistory.push('/inspection-reports')
          } else if (result.data.data.user.role === 'driver') {
            //default route for Customer
            browserHistory.push('/trucker-logs/' + result.data.data.user._id)
          }
          resolve(result.data.data);
        }).catch((error) => {
        reject(false);
        //error.response.data.message
        if (error.response) {
          notify.show(error.response.data.message, 'error');
        }
      })
    })
  }
}
export function updateUserTypeGlobal() {
  return function(dispatch) {
    dispatch({
      type: "UPDATE_TYPE",
      payload: getStorage('varified') === 'true' ? getStorage('currentUserRole') : null,
    });
  }
}
export function destroyLoginSession() {
  return function(dispatch) {
    window.Raven.setUserContext()
    dispatch(actionCreators.success(''));
  }
}
