import axios from 'axios';
import { notify } from 'react-notify-toast';
import { browserHistory } from 'react-router';
import store from './../../redux/store';
import { updateUserType } from '../Login/actionCreators';

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.status === 401) {
        localStorage.clear();
        notify.show('Something went wrong. Please login again.', 'error');
        store.dispatch(updateUserType(null))
        browserHistory.push('/');
    }
    else if(error.status >= 500){
        window.Raven.captureException(error.data.message);
    } else {
        notify.show(error.response ? error.response.data.message : error.data.message, 'error');
        return Promise.reject(error);
    }

});