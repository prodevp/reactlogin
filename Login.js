import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getStorage } from '../../services';
import FormErrors from '../../Components/Common/Validation/FormErrors.js';
import Validation from '../../Components/Common/Validation/Validation.js';
import Message from '../../Components/Common/Message/';
import { Spinner } from '../../Components/Common/Components/Component';
import { browserHistory, Link } from 'react-router'
import * as actionCreators from './actionCreators';
import './style.css';
import { Hints } from 'intro.js-react';



class Login extends Component {
  constructor(props) {
    super(props);

    if (getStorage('token') != null) {
      this.props.actions.updateUserTypeGlobal();
      if (getStorage('currentUserRole') == 'shipper') {
        browserHistory.push('/alltrucks')
      } else if (getStorage('currentUserRole') == 'carrier') {
        browserHistory.push('/truck-shipment-unassigned')
      }
    }

    this.state = {
      email: '',
      password: '',
      role: 'carrier',
      deviceType: 'IOS',
      emailInvalid: false,
      passwordInvalid: false,
      formValid: false,
      processing: false
    }
    this.handleUserInput = this.handleUserInput.bind(this);
    this.changeUser = this.changeUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.goToForgot = this.goToForgot.bind(this);
  }

  goToForgot() {
    browserHistory.push('/getresetToken');
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.formValid) {
      this.setState({
        processing: true
      });
      const {email, password, role, deviceType} = this.state;
      this.props.actions.processForm({
        email,
        password,
        role,
        deviceType,
        flushPreviousSessions: true
      })
        .then((result) => {
          window.Raven.setUserContext({
            email: result.user.email,
            id: result.user._id
          })
        })
        .catch((err) => {
          this.setState({
            processing: false
          });
        })
    }
  }


  changeUser(type) {
    this.setState({
      role: type
    });
  //  this.props.userType = (type == 'carrier' ? 'trucker' : type);
  // this.props.actions.updateUserType(type);
  }

  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value
    });
    const isValid = Validation(name, value);
    if (!isValid) {
      this.setState({
        [`${name}Invalid`]: true
      });
    } else {
      this.setState({
        [`${name}Invalid`]: false
      })
    }

    if (!this.state.emailInvalid && !this.state.passwordInvalid) {
      this.setState({
        formValid: true
      });
    } else {
      this.setState({
        formValid: false
      });
    }
  }

  render() {

    return (
      <div className="row center-child">

        <div className="col-xs-12 col-sm-6 col-md-5">

          <div className="sign-up-form">

            <div className="row">
              <div className="col-sm-12 text-center">
                <h4 className="heading text-uppercase">Log in</h4>
              </div>
            </div>
            <div className="row inline-center-children">
              <div className="col-sm-12">
                <div className="btn-group swith-block" role="group">
                  <button className={[`btn rounded-0 text-uppercase clickable ${this.state.role === 'carrier' ? 'active' : ''}`]} onClick={() => this.changeUser('carrier')} >Trucker</button>
                  <button className={[`btn rounded-0 text-uppercase clickable ${this.state.role === 'shipper' ? 'active' : ''}`]} onClick={() => this.changeUser('shipper')}>Shipper</button>
                  <button className={[`btn rounded-0 text-uppercase clickable ${this.state.role === 'driver' ? 'active' : ''}`]} onClick={() => this.changeUser('driver')}>Driver</button>
                  <button className={[`btn rounded-0 text-uppercase clickable ${this.state.role === 'customer' ? 'active' : ''}`]} onClick={() => this.changeUser('customer')}>Customer</button>
                </div>
              </div>

            </div>
            <div className="">
              <FormErrors formErrors={this.state.formErrors} />
            </div>
            <form className="form">
              <div>
                <div className="row">
                  <div className="col-sm-12">
                    <div className={`form-group ${this.state.emailInvalid ? 'has-error' : ''}`}>
                      <label htmlFor="email" className="text-uppercase">
                        Email address
                            <span className="required-field" title="This field is required." >*</span>
                      </label>
                      <input type="email" className="form-control rounded-0" id="email" name="email" value={this.state.email} onChange={(event) => this.handleUserInput(event)} />
                      {this.state.emailInvalid &&
      <span className="text-danger">{Message.validation.email}</span>
      }
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12">
                    <div className={`form-group ${this.state.passwordInvalid ? 'has-error' : ''}`}>
                      <label htmlFor="password" className="text-uppercase">
                        Password
                            <span className="required-field" title="This field is reqired.">*</span>
                      </label>
                      <input type="password" className="form-control rounded-0" id="password" name="password" value={this.state.password} onChange={(event) => this.handleUserInput(event)} />
                      {this.state.passwordInvalid &&
      <span className="text-danger">{Message.validation.password}</span>
      }
                    </div>
                  </div>
                </div>
                {this.state.processing &&
      <div className="row">
                    <div className="col-sm-12 text-center">
                      <Spinner />
                    </div>
                  </div>
      }

                <div className="row">
                  <div className="col-sm-12">
                    <p className="agree-to-terms">By Logging in, you agree to Axle's <u><Link to="/privacy-policy" >Privacy Policy</Link></u> and <u><Link to="/terms-conditions" >Terms & Conditions</Link></u></p>
                  </div>
                </div>


                <div className="row login-btn">

                  <div className="col-sm-12 inline-center-children">
                    <div className="btn-group">
                      <input type="submit" name="login" id="login" disabled={!this.state.formValid} className="text-uppercase btn-submit clickable" value="Log in" onClick={(e) => {
        this.handleSubmit(e)
      }} />
                    </div>
                  </div>

                </div>

                <div className="row inline-center-children">
                  <a onClick={() => this.goToForgot()} className="already-am forget-password-text">Forgot your password?</a>
                </div>


                <div className="row pt-2">
                  <div className="col-sm-12 text-center">
                    <p className="already-am">Don't have an account? <Link to="/register" >Sign up here</Link></p>
                  </div>
                </div>

              </div>

            </form>

          </div>

        </div>

      </div>

    )
  }
}
;

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    userType: state.HomeReducer.userType,
    user: state.LoginReducer
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);
